
##### Core scientific packages
jupyter==1.0.0
numpy==1.16.2
pandas==0.24.1
scipy==1.2.1
matplotlib==3.0.3


##### Machine Learning packages
scikit-learn==0.20.3
