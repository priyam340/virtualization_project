# Create Dockerfile here
FROM python:slim

WORKDIR /usr/src/app

# Override the nginx start from the base container
COPY *.* ./
COPY requirements.txt ./
COPY start.sh /start.sh 
RUN chmod +x /start.sh \
&& pip install -r requirements.txt
ENTRYPOINT ["/start.sh"]
